package com.example;

import java.util.Random;

/*
ArrayIndexOutOfBoundsException
выход индекса за границу массива
 */

public class AIOOBException {
    public AIOOBException() {
        Random random = new Random();
        int[] arr = new int[5];

        for (int i = 0; i <= arr.length; i++) {
            try {
                arr[i] = random.nextInt(100);
                System.out.println(arr[i]);
            } catch (Exception ex) {
                System.out.println(ex.toString() + "\n");
                break;
            }

        }
    }
}
