package com.example;

/*
NullPointerException
неверное использование пустой ссылки
 */

public class NPException {
    public NPException(){
        String ptr = "слово";

        // проверим, равен ли ptr нулю или работает нормально.
        try
        {
            /*
            Эта строка кода выбрасывает NullPointerException,
            потому что ptr нуль
             */
            if (ptr.equals("строка"))
                System.out.print("Одинаковые");
            else
                System.out.print("Не одинаковые");
        }
        catch(Exception ex)
        {
            System.out.println(ex.toString() + "\n");
        }
    }
}
