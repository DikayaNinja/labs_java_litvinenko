package com.example;

/*
ClassCastException - неверное приведение
 */

public class CCException {
    public CCException(){
        try
        {
            Object i = 42;
            String s = (String)i;
            //System.out.println((Byte)ch);
        }
        catch(Exception ex)
        {
            System.out.println(ex.toString() + "\n");
        }
    }
}
