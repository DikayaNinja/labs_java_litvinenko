package com.example;

/*
ArrayStoreException - присваивание элементу
массива объекта несовместимого типа
 */

public class ArrStException {
    public ArrStException(){
        Object[] szStr = new String[10];
        try
        {
            szStr[0] = new Other('*');
        }
        catch(Exception ex)
        {
            System.out.println(ex.toString() + "\n");
        }
    }
}
