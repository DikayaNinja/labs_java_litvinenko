package com.example;

/*
ArithmeticException - арифметическая ошибка,
например, деление на нуль
 */

public class ArithException {
    public ArithException(){
        try{
            int n1 = 30;
            int output =n1 / 0;
            System.out.println ("Result: "+output);
        }
        catch(Exception ex)
        {
            System.out.println(ex.toString() + "\n");
        }
    }

}
