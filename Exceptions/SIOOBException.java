package com.example;

/*
StringIndexOutOfBounds - попытка использования
индекса за пределами строки
 */

public class SIOOBException {
    public SIOOBException(){
        String str="Литвиненко";

        try{
            System.out.println(str.length());
            char chr = str.charAt(40);
            System.out.println(chr);
        }catch(Exception ex){
            System.out.println(ex.toString() +"\n");
        }
    }
}
