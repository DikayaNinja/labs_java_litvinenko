package example.com;

import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Dancer dancer = new Dancer("Антон", 18);

        // восходящее преобразование к базовому типу
        Dancer breakDanceDancer = new BreakDankDancer("Юра", 19);
        Dancer electricBoogieDancer = new ContemporaryDancer("Алиса", 20);

        List<Dancer> discotheque = Arrays.asList(dancer, breakDanceDancer, electricBoogieDancer);
        for (Dancer d : discotheque) {
            d.dance();// полиморфный вызов метода
        }
    }
}
