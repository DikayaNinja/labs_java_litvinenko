package example.com;

public class ContemporaryDancer extends Dancer {
    public ContemporaryDancer(String name, int age) {
        super(name, age);
    }
    // переопределение метода базового класса
    @Override
    public void dance() {
        System.out.println( toString() + "Я танцую контемп!");
    }
}
