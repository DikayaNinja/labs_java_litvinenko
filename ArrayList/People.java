package com.example;

import java.util.ArrayList;

public class People {
    public static void main(String[] args) {

        ArrayList<String> people = new ArrayList<String>();

        // добавим в список ряд элементов
        people.add("Вася");
        people.add("Алиса");
        people.add("Катя");
        people.add("Саша");
        people.add(1, "Глеб"); // добавляем элемент по индексу 1

        System.out.println(people.get(1));// получаем 2-й объект
        people.set(1, "Вова"); // установка нового значения для 2-го объекта

        System.out.printf("Список содержит %d элементов: \n", people.size());
        for(String person : people){

            System.out.println(person);
        }

        // проверяем наличие элемента
        if(people.contains("Вася")){

            System.out.println("В списке есть Вася ");
        }

        // удаление конкретного элемента
        people.remove("Вова");

        // удаление по индексу
        people.remove(0);

        Object[] peopleArray = people.toArray();
        for(Object person : peopleArray){

            System.out.println(person);
        }
    }
}
