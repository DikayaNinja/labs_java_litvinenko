package com.example;

import java.util.Scanner;

public class Test {
    // функция main() тестирует работу классов Triangle и TriangleException
    public static void main(String[] args) {
        double a, b, c;
        Scanner in = new Scanner(System.in);

        // ввод a, b, c
        System.out.print("a = ");
        a = in.nextDouble();
        System.out.print("b = ");
        b = in.nextDouble();
        System.out.print("c = ");
        c = in.nextDouble();

        // вычисление площади
        Triangle tr = new Triangle(a, b, c);
        double area = tr.getArea();
        System.out.println("area = " + area);

        in.close();
    }
}
