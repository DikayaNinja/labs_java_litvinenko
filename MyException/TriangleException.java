package com.example;
/*
 Создание собственного класса исключения TriangleException
 */
public class TriangleException extends Exception {
    // переопределить метод toString(), описывающий исключение
    public String toString() {
        return "Ошибка. Неверные стороны треугольника.";
    }
}
