package com.example;
/*
 Создание собственного класса Triangle,
 реализующего треугольник по его сторонам
 */
public class Triangle {
    // стороны треугольника
    private double a, b, c;

    // конструктор по умолчанию
    public Triangle() {
        // равносторонний треугольник с длиной стороны 1
        a = b = c = 1;
    }

    // параметризированный конструктор
    public Triangle(double a1, double b1, double c1) {
        // использование класса исключения TriangleException
        try {
            // можно ли из сторон _a, _b, _c создать треугольник
            if (((a1 + b1) < c1)||((a1 + c1) < b1)||((b1 + c1) < a1))
                throw new TriangleException();
        }
        catch(TriangleException e) {
            System.out.println("Exception: "+e.toString());
            return;
        }

        // если стороны треугольника введены корректно,
        // записать их во внутренние переменные класса
        a = a1;
        b = b1;
        c = c1;
    }
    // метод возвращающий площадь треугольника
    public double getArea() {
        // если стороны треугольника имеют корректные размеры,
        // то проверку на корень из отрицательного числа делать не нужно
        double p, s;
        p = (a + b + c) / 2; // полупериметр
        s = Math.sqrt(p * (p - a) * (p - b) * (p - c)); // формула Герона
        return s;
    }
}
